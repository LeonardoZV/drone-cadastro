package tech.mastertech.itau.drone.cadastro.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.drone.cadastro.dto.Endereco;
import tech.mastertech.itau.drone.cadastro.dto.Login;
import tech.mastertech.itau.drone.cadastro.dto.Usuario;
import tech.mastertech.itau.drone.cadastro.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private EnderecoService enderecoService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	private Endereco endereco;
	
	public Usuario cadastrarUsuario(Usuario usuario) {
		endereco = enderecoService.cadastraEndereco(usuario.getEndereco());
		usuario.setEndereco(endereco);
		String senhaCriptografada = passwordEncoder.encode(usuario.getSenha());
	    usuario.setSenha(senhaCriptografada);		
	    return usuarioRepository.save(usuario);
	}
	
	public Optional<Usuario> login(Login login){
	    Optional<Usuario> optional = usuarioRepository.findByEmail(login.getEmail());
	    
	    if(!optional.isPresent()) {
	      return optional;
	    }
	    
	    Usuario usuario = optional.get();
	    
	    if(passwordEncoder.matches(login.getSenha(), usuario.getSenha())) {
	      return optional;
	    }
	    
	    return Optional.empty();
	}
}
