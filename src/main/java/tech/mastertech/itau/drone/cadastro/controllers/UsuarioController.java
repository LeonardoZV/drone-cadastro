package tech.mastertech.itau.drone.cadastro.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.drone.cadastro.dto.Login;
import tech.mastertech.itau.drone.cadastro.dto.RespostaLogin;
import tech.mastertech.itau.drone.cadastro.dto.Usuario;
import tech.mastertech.itau.drone.cadastro.security.JwtTokenProvider;
import tech.mastertech.itau.drone.cadastro.services.UsuarioService;

@RestController
@CrossOrigin
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioservice;

	@Autowired
	private JwtTokenProvider tokenProvider;
	
	@PostMapping("/usuario")
	public Usuario setUsuario(@RequestBody Usuario usuario) {
		return usuarioservice.cadastrarUsuario(usuario);
	}
	
	@PostMapping("/login")
	public RespostaLogin login(@Valid @RequestBody Login login) {
		Optional<Usuario> optional = usuarioservice.login(login);
	   
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN);
		}
	    
		Usuario usuario = optional.get();
		String idUsuario = String.valueOf(usuario.getIdUsuario());
		String token = tokenProvider.criarToken(idUsuario);
	    
		RespostaLogin resposta = new RespostaLogin();
		resposta.setUsuario(usuario);
		resposta.setToken(token);
	    
		return resposta;
	}
}
