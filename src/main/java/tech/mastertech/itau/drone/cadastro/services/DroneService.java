package tech.mastertech.itau.drone.cadastro.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.drone.cadastro.dto.Drone;
import tech.mastertech.itau.drone.cadastro.repositories.DroneRepository;

@Service
public class DroneService {
	
	@Autowired
	private DroneRepository droneRepository;
	
	public Drone cadastrarDrone(Drone drone) {
		return droneRepository.save(drone);
	}
	
	public Optional<Drone> obterDrone(int id) {
		return droneRepository.findById(id);
	}
	
	public Drone atualizarDrone(Drone drone) {
		Optional<Drone> droneOptional = droneRepository.findById(drone.getIdDrone());
		if(droneOptional.isPresent()) {
			Drone droneObtido = droneOptional.get();
			droneObtido.setLatitude(drone.getLatitude());
			droneObtido.setLongitude(drone.getLongitude());
			return droneRepository.save(droneObtido);
		}
		return null;
	}

}
