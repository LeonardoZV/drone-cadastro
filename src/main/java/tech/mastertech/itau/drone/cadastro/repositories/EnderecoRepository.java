package tech.mastertech.itau.drone.cadastro.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.drone.cadastro.dto.Endereco;

public interface EnderecoRepository extends CrudRepository<Endereco, Integer> {

}
