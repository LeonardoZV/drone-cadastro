package tech.mastertech.itau.drone.cadastro.dto;

public enum Perfil {
	ADMIN,
	CLIENTE;
}
