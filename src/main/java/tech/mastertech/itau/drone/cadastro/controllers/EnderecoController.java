package tech.mastertech.itau.drone.cadastro.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.drone.cadastro.dto.Endereco;
import tech.mastertech.itau.drone.cadastro.services.EnderecoService;

@RestController
@CrossOrigin
public class EnderecoController {

	@Autowired
	EnderecoService enderecoService;
	
	@PostMapping("/endereco")
	public Endereco cadastraEndereco(@RequestBody Endereco endereco) {
		return enderecoService.cadastraEndereco(endereco);
	}
	
	@GetMapping("/endereco/{id}")
	public Optional<Endereco> consultaEndereco(@PathVariable int id) {
		return enderecoService.consultaEndereco(id);
	}
	
}
