package tech.mastertech.itau.drone.cadastro.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tech.mastertech.itau.drone.cadastro.dto.Drone;
import tech.mastertech.itau.drone.cadastro.services.DroneService;

@RestController
@CrossOrigin
public class DroneController {
	
	@Autowired
	private DroneService droneService;
	
	@PostMapping("/drone")
	public Drone cadastrarDrone(@RequestBody Drone drone) {
		return droneService.cadastrarDrone(drone);
	}
	
	@GetMapping("/drone/{id}")
	public ResponseEntity<Drone> getDrone(@PathVariable int id) {
		Optional<Drone> drone = droneService.obterDrone(id);		
		if(!drone.isPresent()) {
			return ResponseEntity.notFound().build();
		}		
		return ResponseEntity.ok(drone.get());
	}
	
	@PostMapping("/drone/atualizar")
	public ResponseEntity<Drone> atualizarDrone(@RequestBody Drone drone) {
		Drone droneAtualizado = droneService.atualizarDrone(drone);
		if(droneAtualizado == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(droneAtualizado);
	}
}
