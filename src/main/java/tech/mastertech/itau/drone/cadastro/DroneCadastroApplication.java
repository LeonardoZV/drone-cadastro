package tech.mastertech.itau.drone.cadastro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroneCadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(DroneCadastroApplication.class, args);
	}

}
