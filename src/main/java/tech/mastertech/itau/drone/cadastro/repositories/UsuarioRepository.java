package tech.mastertech.itau.drone.cadastro.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.drone.cadastro.dto.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
	Optional<Usuario> findByEmail(String email);
}
