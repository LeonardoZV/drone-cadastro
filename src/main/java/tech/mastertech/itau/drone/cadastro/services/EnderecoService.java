package tech.mastertech.itau.drone.cadastro.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.drone.cadastro.dto.Endereco;
import tech.mastertech.itau.drone.cadastro.repositories.EnderecoRepository;

@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository enderecoRepository;

	public Endereco cadastraEndereco(Endereco endereco) {

		enderecoRepository.save(endereco);
		return endereco;
	}

	public Optional<Endereco> consultaEndereco(int id) {

		return enderecoRepository.findById(id);

	}

}
