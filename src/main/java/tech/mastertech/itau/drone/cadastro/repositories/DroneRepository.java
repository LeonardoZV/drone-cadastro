package tech.mastertech.itau.drone.cadastro.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.drone.cadastro.dto.Drone;

public interface DroneRepository extends CrudRepository<Drone, Integer>{

}
