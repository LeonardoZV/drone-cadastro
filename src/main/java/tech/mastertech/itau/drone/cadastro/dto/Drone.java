package tech.mastertech.itau.drone.cadastro.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Drone {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDrone;
	@NotBlank
	private String modelo;
	@NotBlank
	private String latitude;
	@NotBlank
	private String longitude;

	public int getIdDrone() {
		return idDrone;
	}

	public void setIdDrone(int idDrone) {
		this.idDrone = idDrone;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}
