package tech.mastertech.itau.drone.cadastro.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.drone.cadastro.controllers.DroneController;
import tech.mastertech.itau.drone.cadastro.dto.Drone;
import tech.mastertech.itau.drone.cadastro.security.JwtTokenProvider;
import tech.mastertech.itau.drone.cadastro.services.DroneService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = DroneController.class)
public class DroneControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private DroneService droneService;
	@MockBean
	private JwtTokenProvider tokenProvider;

	private ObjectMapper mapper = new ObjectMapper();
	private Drone drone;

	@Before
	public void preparar() {
		drone = new Drone();
		drone.setModelo("modelo");
		drone.setLatitude("12345");
		drone.setLongitude("54321");
	}

	@Test
	@WithMockUser(username= "1")
	public void deveCadastrarUmDrone() throws Exception {

		String droneJson = mapper.writeValueAsString(drone);
		System.out.println(droneJson);

		when(droneService.cadastrarDrone(any(Drone.class))).then(answer -> answer.getArgument(0));
		
		mockMvc.perform(post("/dronedelivery/drone")
				.content(droneJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(containsString(droneJson)));
	}

	@Test
	@WithMockUser(username= "1")
	public void deveRetornarUmDronePeloId() throws Exception {
		int id = 1;

		when(droneService.obterDrone(id)).thenReturn(Optional.of(drone));

		mockMvc.perform(get("/dronedelivery/drone/" + id)).andExpect(status().isOk());
	}
	
	@Test
	@WithMockUser(username= "1")
	public void naoDeveRetornarUmDronePeloIdInexistente() throws Exception {
		int id = 0;

		when(droneService.obterDrone(id)).thenReturn(Optional.empty());

		mockMvc.perform(get("/dronedelivery/drone/" + id)).andExpect(status().isNotFound());
	}
	
	@Test
	@WithMockUser(username= "1")
	public void deveAtualizarAPosicaoDeUmDrone() throws Exception {

		drone.setIdDrone(1);
		drone.setModelo("modeloalterado");
		drone.setLatitude("111");
		drone.setLongitude("222");

		String droneJson = mapper.writeValueAsString(drone);

		when(droneService.atualizarDrone(any(Drone.class))).then(answer -> answer.getArgument(0));

		mockMvc.perform(post("/dronedelivery/drone/atualizar").content(droneJson).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string(containsString(droneJson)));
	}
	
}
