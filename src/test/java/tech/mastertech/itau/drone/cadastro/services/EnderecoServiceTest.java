package tech.mastertech.itau.drone.cadastro.services;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.drone.cadastro.dto.Endereco;
import tech.mastertech.itau.drone.cadastro.repositories.EnderecoRepository;
import tech.mastertech.itau.drone.cadastro.services.EnderecoService;

@RunWith(SpringRunner.class) 
@ContextConfiguration(classes = EnderecoService.class) 
public class EnderecoServiceTest {
	
	private Endereco endereco;
	
	@Autowired
	EnderecoService enderecoService;

	@MockBean
	EnderecoRepository enderecoRepository;
	
	@Before
	public void inicializaOsTestes() {
		
		endereco = new Endereco();
		endereco.setBairro("Mooca");
		endereco.setCep("03113786");
		endereco.setLogradouro("Rua do Limao");
		endereco.setCidade("São Paulo");
		endereco.setEstado("SP");
		endereco.setIdEndereco(1);
		endereco.setLatitude("x23");
		endereco.setLongitude("y40");
		endereco.setNumero(1400);
		endereco.setPais("Brasil");
		endereco.setComplemento("Apto. 332");
		
	}
	
	@Test
	public void deveCadastrarUmEndereco() {
		
		Mockito.when(enderecoRepository.save(endereco)).thenReturn(endereco);
		
		Endereco enderecoDeRetorno = enderecoService.cadastraEndereco(endereco);
		
		Assert.assertEquals(endereco.getBairro(), enderecoDeRetorno.getBairro());
		
	}
	
	//Aceitou um dado nulo
/*	@Test
	public void deveCadastrarUmEnderecoIncompleto() {
		
		endereco.setCep(null);
		
		Mockito.when(enderecoRepository.save(endereco)).thenReturn(endereco);
		
		Endereco enderecoDeRetorno = enderecoService.cadastraEndereco(endereco);
		
		Assert.assertEquals("03113010", enderecoDeRetorno.getCep());
		
	}*/

	@Test
	public void deveConsultarUmEndereco() {
		
		Mockito.when(enderecoRepository.findById(endereco.getIdEndereco()))
		.thenReturn(Optional.of(endereco));
		
		Optional<Endereco> optional = enderecoService.consultaEndereco(endereco.getIdEndereco());
		
		Endereco enderecoDeRetorno = optional.get();
		
		Assert.assertEquals(endereco.getBairro(), enderecoDeRetorno.getBairro());
		
	}
	
	@Test
	public void deveConsultarUmEnderecoInexistente() {
		
		Mockito.when(enderecoRepository.findById(endereco.getIdEndereco()))
		.thenReturn(Optional.empty());
		
		Optional<Endereco> optional = enderecoService.consultaEndereco(endereco.getIdEndereco());
		
		
		Assert.assertFalse(optional.isPresent());
		
		
	}

	
}
