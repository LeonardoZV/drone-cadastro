package tech.mastertech.itau.drone.cadastro.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.drone.cadastro.controllers.UsuarioController;
import tech.mastertech.itau.drone.cadastro.dto.Endereco;
import tech.mastertech.itau.drone.cadastro.dto.Login;
import tech.mastertech.itau.drone.cadastro.dto.RespostaLogin;
import tech.mastertech.itau.drone.cadastro.dto.Usuario;
import tech.mastertech.itau.drone.cadastro.security.JwtTokenProvider;
import tech.mastertech.itau.drone.cadastro.services.UsuarioService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UsuarioController.class)
public class UsuarioControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UsuarioService usuarioService;
	
	@MockBean
	private JwtTokenProvider tokenProvider;
	
	ObjectMapper mapper = new ObjectMapper();
	
	private Usuario usuario = new Usuario();
	private String emailValido = "usuario@teste.com";
	  
	@Before
	public void preparar() {
	    usuario = new Usuario();
	    
	    usuario.setNome("José Cliente");
	    usuario.setEmail(emailValido);
	    usuario.setSenha("umaSenha123");
	}
	
	@Test
	@WithMockUser(username= "1")
	public void deveCadastrarUsuario() throws Exception {

		Endereco endereco = new Endereco();		
		endereco.setBairro("Mooca");
		endereco.setCep("03113786");
		endereco.setLogradouro("Rua do Limao");
		endereco.setCidade("São Paulo");
		endereco.setEstado("SP");
		endereco.setIdEndereco(1);
		endereco.setLatitude("x23");
		endereco.setLongitude("y40");
		endereco.setNumero(1400);
		endereco.setPais("Brasil");
		endereco.setComplemento("Apto. 332");

		Usuario usuarioCadastrado = new Usuario();
		usuarioCadastrado.setIdUsuario(1);
		usuarioCadastrado.setEmail("a@a.com.br");
		usuarioCadastrado.setSenha("1234");
		usuarioCadastrado.setNome("Leonardo");
		usuarioCadastrado.setTelefone("955590404");
		usuarioCadastrado.setEndereco(endereco);

		String json = mapper.writeValueAsString(usuarioCadastrado);
		
		Mockito.when(usuarioService.cadastrarUsuario(any(Usuario.class))).thenReturn(usuarioCadastrado);
				
		mockMvc.perform(MockMvcRequestBuilders.post("/dronedelivery/usuario")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON_UTF8))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string(json));
	}
	
	@Test
	public void deveLogarUmUsuario() throws Exception {
	    usuario.setIdUsuario(1);
	    
	    String token = "umTokenJwt";
	    String idUsuario = String.valueOf(usuario.getIdUsuario());
	    
	    Login login = new Login();
	    login.setEmail(emailValido);
	    login.setSenha("umaSenha123");
	    
	    RespostaLogin resposta = new RespostaLogin();
	    resposta.setUsuario(usuario);
	    resposta.setToken(token);
	    
	    when(usuarioService.login(any(Login.class))).thenReturn(Optional.of(usuario));
	    when(tokenProvider.criarToken(idUsuario)).thenReturn(token);
	    
	    String loginJson = mapper.writeValueAsString(login);
	    String respostaJson = mapper.writeValueAsString(resposta);
	    
	    mockMvc.perform(post("/dronedelivery/login")
	            .contentType(MediaType.APPLICATION_JSON_UTF8)
	            .content(loginJson))
	        .andExpect(status().isOk())
	        .andExpect(content().string(containsString(respostaJson)))
	        .andExpect(content().string(containsString("token")));
	}
	  
	@Test
	public void deveRetornarErroEmLoginInvalido() throws Exception {
	    Login login = new Login();
	    login.setEmail(emailValido);
	    login.setSenha("umaSenha123");
	    
	    when(usuarioService.login(any(Login.class))).thenReturn(Optional.empty());
	    
	    String loginJson = mapper.writeValueAsString(login);
	    
	    mockMvc.perform(post("/dronedelivery/login")
	            .contentType(MediaType.APPLICATION_JSON_UTF8)
	            .content(loginJson))
	        .andExpect(status().isForbidden());
	}	
}
