package tech.mastertech.itau.drone.cadastro;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.drone.cadastro.DroneCadastroApplication;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DroneCadastroApplicationTests {

	@Test
	public void contextLoads() {
		DroneCadastroApplication.main(new String[0]);
	}

}
