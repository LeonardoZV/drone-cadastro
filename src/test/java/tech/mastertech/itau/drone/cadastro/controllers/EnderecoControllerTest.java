package tech.mastertech.itau.drone.cadastro.controllers;

import static org.junit.Assert.fail;

import java.util.Optional;

import org.apache.catalina.mapper.Mapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.drone.cadastro.controllers.EnderecoController;
import tech.mastertech.itau.drone.cadastro.dto.Endereco;
import tech.mastertech.itau.drone.cadastro.services.EnderecoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = EnderecoController.class)
public class EnderecoControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private EnderecoService enderecoService;

	ObjectMapper mapper;

	Endereco endereco;

	@Before
	public void inicializaOsTestes() {

		endereco = new Endereco();
		mapper = new ObjectMapper();
		endereco.setBairro("Mooca");
		endereco.setCep("03113786");
		endereco.setLogradouro("Rua do Limao");
		endereco.setCidade("São Paulo");
		endereco.setEstado("SP");
		endereco.setIdEndereco(1);
		endereco.setLatitude("x23");
		endereco.setLongitude("y40");
		endereco.setNumero(1400);
		endereco.setPais("Brasil");
		endereco.setComplemento("Apto. 332");

	}

	@Test
	@WithMockUser
	public void deveCadastrarEndereco() throws Exception {

		String enderecoJson = mapper.writeValueAsString(endereco);

		Mockito.when(enderecoService.cadastraEndereco(ArgumentMatchers.any(Endereco.class))).thenReturn(endereco);

		mockMvc.perform(MockMvcRequestBuilders.post("/dronedelivery/endereco").content(enderecoJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string(enderecoJson));

	}

	@Test
	@WithMockUser
	public void deveConsultarEndereco() throws Exception {

		String enderecoJson = mapper.writeValueAsString(endereco);

		Mockito.when(enderecoService.consultaEndereco(endereco.getIdEndereco()))
		.thenReturn(Optional.of(endereco));

		mockMvc.perform(MockMvcRequestBuilders.get("/dronedelivery/endereco/" + endereco.getIdEndereco()).content(enderecoJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().string(enderecoJson));

	}

}
