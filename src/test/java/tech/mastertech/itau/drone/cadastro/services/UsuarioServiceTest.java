package tech.mastertech.itau.drone.cadastro.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.drone.cadastro.dto.Endereco;
import tech.mastertech.itau.drone.cadastro.dto.Login;
import tech.mastertech.itau.drone.cadastro.dto.Usuario;
import tech.mastertech.itau.drone.cadastro.repositories.EnderecoRepository;
import tech.mastertech.itau.drone.cadastro.repositories.UsuarioRepository;
import tech.mastertech.itau.drone.cadastro.services.EnderecoService;
import tech.mastertech.itau.drone.cadastro.services.UsuarioService;

@RunWith(SpringRunner.class) 
@ContextConfiguration(classes = { UsuarioService.class, EnderecoService.class }) 
public class UsuarioServiceTest {

	@Autowired
	private UsuarioService usuarioService;
	
	@MockBean
	private UsuarioRepository usuarioRepository;

	@MockBean
	private EnderecoRepository enderecoRepository;
	
	@MockBean
	private BCryptPasswordEncoder passwordEncoder;
	  
	private Usuario usuario;
	private Login login;
	private String email = "usuario@teste.com";
	private String senha = "minhaSenha123";
	private String senhaCriptografada = "senhaCriptografada";
	  
	@Before
	public void preparar() {
	    usuario = new Usuario();
	    usuario.setNome("José Cliente");
	    usuario.setEmail(email);
	    usuario.setSenha(senha);
	    
	    login = new Login();
	    login.setEmail(email);
	    login.setSenha(senha);
	}
	
	@Test
	public void deveCadastrarUmUsuario() {
		
		Endereco endereco = new Endereco();		
		endereco.setBairro("Mooca");
		endereco.setCep("03113786");
		endereco.setLogradouro("Rua do Limao");
		endereco.setCidade("São Paulo");
		endereco.setEstado("SP");
		endereco.setIdEndereco(1);
		endereco.setLatitude("x23");
		endereco.setLongitude("y40");
		endereco.setNumero(1400);
		endereco.setPais("Brasil");
		endereco.setComplemento("Apto. 332");

		Usuario usuarioCadastrar = new Usuario();
		usuarioCadastrar.setEmail("a@a.com.br");
		usuarioCadastrar.setSenha("1234");
		usuarioCadastrar.setNome("Leonardo");
		usuarioCadastrar.setTelefone("955590404");
		usuarioCadastrar.setEndereco(endereco);
		
		Usuario usuarioCadastrado = new Usuario();
		usuarioCadastrado.setIdUsuario(1);
		usuarioCadastrado.setEmail("a@a.com.br");
		usuarioCadastrado.setSenha("1234");
		usuarioCadastrado.setNome("Leonardo");
		usuarioCadastrado.setTelefone("955590404");
		usuarioCadastrado.setEndereco(endereco);
		
		Mockito.when(enderecoRepository.save(endereco)).thenReturn(endereco);
		Mockito.when(usuarioRepository.save(usuarioCadastrar)).thenReturn(usuarioCadastrado);
		
		Usuario usuarioRetornado = usuarioService.cadastrarUsuario(usuarioCadastrar);
		
		Assert.assertEquals(usuarioCadastrar.getNome(), usuarioRetornado.getNome());
	}
	
	@Test
	public void deveFazerLoginDeUmUsuario() {   
	    usuario.setSenha(senhaCriptografada);
	    
	    when(usuarioRepository.findByEmail(email)).thenReturn(Optional.of(usuario));
	    when(passwordEncoder.matches(senha, senhaCriptografada)).thenReturn(true);
	    
	    Optional<Usuario> optional = usuarioService.login(login);
	    
	    assertTrue(optional.isPresent());
	    assertEquals(usuario, optional.get());
	}
	  
	@Test
	public void deveFazerRetornarVazioEmErroDeVerificacaoDeSenha() {
	    usuario.setSenha(senhaCriptografada);
	    
	    when(usuarioRepository.findByEmail(email)).thenReturn(Optional.of(usuario));
	    when(passwordEncoder.matches(senha, senhaCriptografada)).thenReturn(false);
	    
	    Optional<Usuario> optional = usuarioService.login(login);
	    
	    assertFalse(optional.isPresent());
	}
	  
	@Test
	public void deveFazerRetornarVazioCasoEmailNaoSejaEncontrado() {
	    usuario.setSenha(senhaCriptografada);
	    
	    when(usuarioRepository.findByEmail(email)).thenReturn(Optional.empty());
	    
	    Optional<Usuario> optional = usuarioService.login(login);
	    
	    assertFalse(optional.isPresent());
	}
}
