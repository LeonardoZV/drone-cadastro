package tech.mastertech.itau.drone.cadastro.services;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.drone.cadastro.dto.Drone;
import tech.mastertech.itau.drone.cadastro.dto.Endereco;
import tech.mastertech.itau.drone.cadastro.repositories.DroneRepository;
import tech.mastertech.itau.drone.cadastro.services.DroneService;

@RunWith(SpringRunner.class) 
@ContextConfiguration(classes = DroneService.class) 
public class DroneServiceTest {
	
	private Drone drone;
	
	@Autowired
	DroneService droneService;

	@MockBean
	DroneRepository droneRepository;
	
	@Before
	public void inicializaOsTestes() {
		drone = new Drone();
		drone.setIdDrone(1);
		drone.setModelo("modelo teste");
		drone.setLatitude("1.000");
		drone.setLongitude("1.000");
	}
	
	@Test
	public void deveCadastrarUmDrone() {
		
		Mockito.when(droneRepository.save(drone)).thenReturn(drone);
		
		Drone droneDeRetorno = droneService.cadastrarDrone(drone);
		
		Assert.assertEquals(drone.getModelo(), droneDeRetorno.getModelo());
		Assert.assertEquals(drone.getLongitude(), droneDeRetorno.getLongitude());
		Assert.assertEquals(drone.getLatitude(), droneDeRetorno.getLatitude());
	}
	
	@Test
	public void deveConsultarUmDronePorId() {
		
		Mockito.when(droneRepository.findById(drone.getIdDrone()))
		.thenReturn(Optional.of(drone));
		
		Optional<Drone> optional = droneService.obterDrone(drone.getIdDrone());
		
		Drone droneDeRetorno = optional.get();
		
		Assert.assertEquals(drone.getModelo(), droneDeRetorno.getModelo());
		Assert.assertEquals(drone.getLongitude(), droneDeRetorno.getLongitude());
		Assert.assertEquals(drone.getLatitude(), droneDeRetorno.getLatitude());
	}
	
	@Test
	public void deveAtualizarUmDrone() {
		Mockito.when(droneRepository.findById(drone.getIdDrone()))
		.thenReturn(Optional.of(drone));
		Mockito.when(droneRepository.save(drone)).thenReturn(drone);
		
		Drone droneDeRetorno = droneService.atualizarDrone(drone);
		
		Assert.assertEquals(drone.getModelo(), droneDeRetorno.getModelo());
		Assert.assertEquals(drone.getLongitude(), droneDeRetorno.getLongitude());
		Assert.assertEquals(drone.getLatitude(), droneDeRetorno.getLatitude());
	}
	
	@Test
	public void naoDeveAtualizarUmDroneInexistente() {
		Mockito.when(droneRepository.findById(0))
		.thenReturn(Optional.empty());
		
		Drone droneDeRetorno = droneService.atualizarDrone(drone);
		
		Assert.assertEquals(null, droneDeRetorno);
	}

}
